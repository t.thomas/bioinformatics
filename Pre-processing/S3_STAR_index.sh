#!/bin/bash
#SBATCH --nodes 1                  
#SBATCH --ntasks 16                 # Number of nodes. Ensure that all cores are on one machine
#SBATCH --time 2-00:0              # Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#LOAD THE MODULES
module load STAR/2.7.6a-GCC-10.2.0
module load GCC/10.2.0
module load zlib/1.2.11-GCCcore-10.2.0

##### STAR - make genome index. Indexing the reference genome only has to be run once. ##########
# create a directory to store the index in 
mkdir STAR_index

# Run STAR to generate a genome index
STAR --runThreadN 16 --runMode genomeGenerate --genomeDir /path/to/STAR_index --genomeFastaFiles /path/to/fasta_file_of_genome_assembly --sjdbGTFfile /path/to/GTF_file_of_genome_annotation --sjdbOverhang 74

# Make sure the --runThreadN is EQUAL to your #SBATCH --ntasks 16
# Access Fasta and GTF files from Ensembl FTP site using a cross platform FTP application such as FileZilla. 

# Run in a SLURM environment.