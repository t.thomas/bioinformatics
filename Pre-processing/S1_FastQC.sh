#!/bin/bash
#SBATCH --nodes 1                  
#SBATCH --ntasks 4                    # Number of nodes. Ensure that all cores are on one machine
#SBATCH --time 0-01:0              # Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#Load modules 
module load FastQC/0.11.5-Java-1.8.0_74
module load Java/11

# go to directory with fastq files
	cd /path/to/directory/

#make a new directory where the fastqc results will be outputted to. 
mkdir fastqc_results

#Fastqc
for file in *.fastq
do
fastqc ${file}  -o /path/to/fastqc_results/
done

# Run in a SLURM environment. 