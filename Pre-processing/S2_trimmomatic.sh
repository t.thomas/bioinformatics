#!/bin/bash
#SBATCH --nodes 1                  
#SBATCH --ntasks 8                 # Number of nodes. Ensure that all cores are on one machine
#SBATCH --time 2-00:0              # Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#Load modules
module load Trimmomatic/0.39-Java-11
module load Java/11

# go to directory with fastq files
	cd /path/to/directory/
	
#make new directory for output files
mkdir trimmed_results

#assign output directory to a variable
output_dir="/path/to/trimmed_results/"
	
### For single-end reads:
	for file in *.fastq
	do
	java -jar /path/to/trimmomatic-0.39.jar SE -phred33 \
	# replace with path to .jar file
	${file} \
	$output_dir/${file%.fastq}_trimmed.fastq \
	ILLUMINACLIP:/path/to/adapters/TruSeq3-SE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
	# replace with path to adapter file and use appropriate parameters for data.
	done

# Remove poly A tails
adapter_seq = "AAAAAAAAAAAA"

for file in *.fastq
do
    base_name="${file%.fastq}"
    
    # Run Trimmomatic on the file
    java -jar /path/to/trimmomatic-0.39.jar SE -phred33 \
    # replace with path to .jar file
    ${file} \
    $output_dir/${base_name}_trimmed.fastq \
    ILLUMINACLIP:<(echo ">Adapter\n$adapter_seq"):2:30:10
done

### For paired-end reads:
	for file in *_1.fastq
	do
    java -jar /path/to/trimmomatic-0.39.jar PE -phred33 \
    # replace with path to .jar file
    ${file} \
    ${file%_1.fastq}_2.fastq \
    $output_dir/${file%_1.fastq}_1_paired.fastq \
    $output_dir/${file%_1.fastq}_1_unpaired.fastq \
    $output_dir/${file%_1.fastq}_2_paired.fastq \
    $output_dir/${file%_1.fastq}_2_unpaired.fastq \
    ILLUMINACLIP:/path/to/adapters/TruSeq3-PE.fa:2:30:10:2:keepBothReads LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
	# replace with path to adapter file and use appropriate parameters for data.
	done
	
# Remove poly A tails
adapter_seq="AAAAAAAAAAAA"

for file in *_1.fastq
do
    base_name="${file%_1.fastq}"

    # Run Trimmomatic on the file
    java -jar /path/to/trimmomatic-0.39.jar PE -phred33 \
     # replace with path to .jar file
    ${file} \
    ${base_name}_2.fastq \
    $output_dir/${base_name}_1_paired.fastq \
    $output_dir/${base_name}_1_unpaired.fastq \
    $output_dir/${base_name}_2_paired.fastq \
    $output_dir/${base_name}_2_unpaired.fastq \
    ILLUMINACLIP:<(echo ">Adapter\n$adapter_seq"):2:30:10
done

# Run in a SLURM environment.


