#!/bin/bash
#SBATCH --nodes 1                   #Number of nodes. Ensure that all cores are on one machine  =1
#SBATCH --ntasks 16                 #Number of CPUs for STAR choose 8-20. 
#SBATCH --time 2-00:0               #Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#LOAD modules
module load HTSeq/0.13.5-foss-2020a-Python-3.8.2
module load foss/2020a
module load matplotlib/3.2.1-foss-2020a-Python-3.8.2
module load Pysam/0.16.0.1-GCC-9.3.0
module load Python/3.8.2-GCCcore-9.3.0
module load SciPy-bundle/2020.03-foss-2020a-Python-3.8.2

# Go to your directory
cd /path/to/BAM_files/

# count reads
htseq-count -m union -s yes -r pos -f bam *.bam /path/to/genome/assemble/gtf/file

# If this is not running, make sure that you dont have any empty bam files in your directory...
