#!/bin/bash
#SBATCH --nodes 1                   #Number of nodes. Ensure that all cores are on one machine  =1
#SBATCH --ntasks 16                 #Number of CPUs for STAR choose 8-20. 
#SBATCH --time 2-00:0               #Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#LOAD THE MODULES 
module load STAR/2.7.6a-GCC-10.2.0
module load GCC/10.2.0
module load zlib/1.2.11-GCCcore-10.2.0

#make new directory for output files
mkdir BAM_files

output_dir="/path/to/BAM_files/"

#### Run STAR to align to the genome. 

### For single-end reads:
for file in *.fastq
do
    base_name="${file%.fastq}"

    STAR --runThreadN 16 --genomeDir /path/to/STAR_index \
    --readFilesIn ${file} \
    --outFileNamePrefix $output_dir/${base_name}_ \
    --outSAMtype BAM SortedByCoordinate
done


### For paired-end reads:

for file in *_1.fastq
do
    base_name="${file%_1.fastq}"

    STAR --runThreadN 16 --genomeDir /path/to/STAR_index \
    --readFilesIn ${file} ${base_name}_2.fastq \
    --outFileNamePrefix $output_dir/${base_name}_ \
    --outSAMtype BAM SortedByCoordinate \
    --twopassMode Basic
done

# Make sure the --runThreadN is EQUAL to your #SBATCH --ntasks 16 

# Run in a SLURM environment.