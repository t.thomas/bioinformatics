#!/bin/bash
#SBATCH --nodes 1                  
#SBATCH --ntasks 16                 # Number of nodes. Ensure that all cores are on one machine
#SBATCH --time 2-00:0              # Runtime in D-HH:MM
#SBATCH -o /path/to/directory/out.txt      # File to which STDOUT will be written
#SBATCH -e /path/to/directory/err.txt      # File to which STDERR will be written
#SBATCH --account=..........

#LOAD THE MODULES
module load kallisto/0.46.1-foss-2019b
module load foss/2019b
module load HDF5/1.10.5-gompi-2019b

# Directory containing your FASTQ files
fastq_dir="/path/to/your/fastq/files"

# Kallisto index 
kallisto_index="/path/to/kallisto_index.idx"

# Output directory
mkdir TPM_files
output_dir="/path/to/TPM_files"


###For single-end reads

# Loop over all FASTQ files in the directory
for fastq_file in $fastq_dir/*.fastq
do
  # Get the base name of the file (without the directory path and extension)
  base_name=$(basename $fastq_file .fastq)

  # Run Kallisto
  kallisto quant -i $kallisto_index -o $output_dir/$base_name $fastq_file
done



### For pair end reads
for fastq_file in $fastq_dir/*_1.fastq
do
  # Get the base name of the file (without the directory path and extension)
  base_name=$(basename $fastq_file _1.fastq)

  # Define the names of the two fastq files for this sample
  fastq_file1=$fastq_dir/${base_name}_1.fastq
  fastq_file2=$fastq_dir/${base_name}_2.fastq

  # Run Kallisto
  kallisto quant -i $kallisto_index -o $output_dir/$base_name -b 100 $fastq_file1 $fastq_file2
done


