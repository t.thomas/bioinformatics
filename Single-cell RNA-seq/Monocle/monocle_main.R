##-----------------------------------------------------------------------------------##
## The following script is a workflow for filtering, normalising, and clustering     ##
## single cell RNA-seq data according to cell type and disease status,               ##
## and constructing a pseudotime line in Monocle.                                    ##
##-----------------------------------------------------------------------------------##

library(rgl)
library(proxy)
library(Rtsne)
library(monocle)
library(mclust)
library(randomForest)
library(ggplot2)
library(devtools)
setwd("C:/Users/....")
path_code = "C:/Users/......"

#loads functions "LoadDataNorm", "Classify_Neuron_nonNeuron", "Classify_MN_nonMN_v4"
source(paste(path_code, "LoadDataNorm.R", sep="/"))
source(paste(path_code, "Classify_Neuron_nonNeuron.R", sep="/"))
source(paste(path_code, "Classify_MN_nonMN_v4.R", sep="/"))

####read in data

############## Filter, normalize and return data packaged in a CDS object ##########

cds_obj = loadDataNorm(sc_df_full, sc_df_fpkm, celldata_df, genedata_df, mapping_info)

########## Convert gene symbols into ensemble ids #####
chat_ens = as.character(fData(cds_obj)$Gene_Id[match("CHAT", fData(cds_obj)$Gene_Name)])
vacht_ens = as.character(fData(cds_obj)$Gene_Id[match("SLC18A3", fData(cds_obj)$Gene_Name)])
isl1_ens = as.character(fData(cds_obj)$Gene_Id[match("ISL1", fData(cds_obj)$Gene_Name)])
mnx1_ens = as.character(fData(cds_obj)$Gene_Id[match("MNX1", fData(cds_obj)$Gene_Name)])
syp_ens = as.character(fData(cds_obj)$Gene_Id[match("SYP", fData(cds_obj)$Gene_Name)])
tuj1_ens = as.character(fData(cds_obj)$Gene_Id[match("TUBB3", fData(cds_obj)$Gene_Name)])
syt1_ens = as.character(fData(cds_obj)$Gene_Id[match("SYT1", fData(cds_obj)$Gene_Name)])
map2_ens = as.character(fData(cds_obj)$Gene_Id[match("MAP2", fData(cds_obj)$Gene_Name)])
gad1_ens = as.character(fData(cds_obj)$Gene_Id[match("GAD1", fData(cds_obj)$Gene_Name)])
gad2_ens = as.character(fData(cds_obj)$Gene_Id[match("GAD2", fData(cds_obj)$Gene_Name)])
sox9_ens = as.character(fData(cds_obj)$Gene_Id[match("SOX9", fData(cds_obj)$Gene_Name)])
s100b_ens = as.character(fData(cds_obj)$Gene_Id[match("S100B", fData(cds_obj)$Gene_Name)])

####### Markers used for classification ########
neuron_markers = c("TUBB3", "SYP", "SYN1", "SYN2", "SYN3", "NEUROG1", "NEUROG2")
non_neuron_markers = c("OLIG1","OLIG2","SOX10","S100B","GFAP","SOX9")
MN_markers = c("CHAT", "SLC18A3", "MNX1", "ISL1")
nonMN_markers = c("SIM1", "VSX2", "GATA2", "GATA3", "EN1", "EVX1", "EVX2", "PAX2", "LHX1", "LHX5", "BHLHE22", "PRDM8", "GAD1", "GAD2", "NKX6-2")

####### Retain markers that pass the filters #######
neuron_markers = intersect(neuron_markers, fData(cds_obj)$Gene_Name)
non_neuron_markers = intersect(non_neuron_markers, fData(cds_obj)$Gene_Name)
MN_markers = intersect(MN_markers, fData(cds_obj)$Gene_Name)
nonMN_markers = intersect(nonMN_markers, fData(cds_obj)$Gene_Name)

##############################################################################################################
############# Classify all cells into neurons and non-neurons #########################################

retList = Classify_Neu_nonNeu(neuron_markers, MN_markers, nonMN_markers, non_neuron_markers, cds_obj)
cds_obj = retList$cds
Neu_nonNeu_de_gene = retList$genes

##############################################################################################################
############# Classify disease cells into motor neurons and non MN using a similar approach as above#########################################
cds_disease = cds_obj[, pData(cds_obj)$Sample=="ND62"]
cds_healthy = cds_obj[, pData(cds_obj)$Sample=="GE62"]

rm(retList)
retList = Classify_MN_nonMN(MN_markers, nonMN_markers, cds_disease)
cds_disease = retList$cds
MN_nonMN_disease_de_gene = retList$genes

#rm(retList)
#retList = Classify_MN_nonMN(MN_markers, nonMN_markers, cds_healthy)
#cds_healthy = retList$cds
#MN_nonMN_ge_de_gene = retList$genes

##### extract only MN and combine ######
disease_mn_lib <- rownames(pData(cds_disease)[which(pData(cds_disease)$NeuType=="MN"),])
healthy_mn_lib <- rownames(pData(cds_healthy)[which(pData(cds_healthy)$NeuType=="MN"), ])
mn_cds_disease_healthy <- cds_obj[,c(disease_mn_lib, healthy_mn_lib)]
#mn_cds_disease <- cds_obj[,(disease_mn_lib)]

############# Create HSMM object from the list of differentially expressed genes ########
########http://cole-trapnell-lab.github.io/monocle-release/docs/#constructing-single-cell-trajectories#####

HSMM_MN_disease <- setOrderingFilter(mn_cds_disease, MN_disease_healthy_de_gene)
plot_ordering_genes(HSMM_MN_disease)

######## reduce data dimensionality #########

HSMM_MN_disease <- reduceDimension(HSMM_MN_disease, max_components = 2,
                            method = 'DDRTree')

########## order cells along the trajectory ############## 
HSMM_MN_disease <- orderCells(HSMM_MN_disease)
plot_cell_trajectory(HSMM_MN_disease, color_by = "State") #nb: state is monocles term for the segment of the tree

##identify the state that contains the most cells from time 0
GM_state <- function(mn_cds_disease){
  if (length(unique(pData(mn_cds_disease)$State)) > 1){
    T0_counts <- table(pData(mn_cds_disease)$State)[,"0"]
    return(as.numeric(names(T0_counts)[which
                                       (T0_counts == max(T0_counts))]))
  } else {
    return (1)
  }
}

HSMM_MN_disease <- orderCells(HSMM_MN_disease, root_state = GM_state(HSMM_MN_disease))
plot_cell_trajectory(HSMM_MN_disease, color_by = "Pseudotime")

##check where each state is located #####
plot_cell_trajectory(HSMM_MN_disease, color_by = "State") +
  facet_wrap(~State, nrow = 1)

######################################### genes pseudotimeline #######################################################

##MN_marker_genes <- row.names(subset(fData(HSMM_MN_disease), 
                                ## Gene_Name %in% c())

sig_gene_names <- MN_disease_healthy_de_gene
plot_pseudotime_heatmap(HSMM_MN_disease[sig_gene_names,],
                        num_clusters = 8,
                        cores = 1,
                        show_rownames = T)

########################################### cell death ###############################

cell_death<-cell_death[-1,]
cell_death<-as.vector(cell_death)

necrotic_cell_death<-necrotic_cell_death[-1,]
necrotic_cell_death<-as.vector(necrotic_cell_death)

cell_death_regulation<-cell_death_regulation[-1,]
cell_death_regulation<- as.vector(cell_death_regulation)

combine<-c(cell_death, necrotic_cell_death, cell_death_regulation)

deathvec.ens<-rownames(fData(mn_cds_disease_healthy))[match(deathvec, fData(mn_cds_disease_healthy)$Gene_Name)]


############# Create HSMM object from the list of cell death ########

HSMM_MN_disease <- setOrderingFilter(mn_cds_disease_healthy, deathvec.ens)
plot_ordering_genes(HSMM_MN_disease)

######## reduce data dimensionality #########

HSMM_MN_disease <- reduceDimension(HSMM_MN_disease, max_components = 2,
                               method = 'DDRTree')


########## order cell death along the trajectory ############## 
HSMM_MN_disease <- orderCells(HSMM_MN_disease)
plot_cell_trajectory(HSMM_MN_disease, color_by = "Sample") #nb: state is monocles term for the segment of the tree; do 'Sample' to pull out GE and ALS

##identify the state that contains the most cells from time 0
GM_state <- function(mn_cds_disease_healthy){
  if (length(unique(pData(mn_cds_disease_healthy)$Sample)) > 1){
    T0_counts <- table(pData(mn_cds_disease_healthy)$Sample)[,"0"]
    return(as.numeric(names(T0_counts)[which
                                       (T0_counts == max(T0_counts))]))
  } else {
    return (1)
  }
}

HSMM_MN_disease <- orderCells(HSMM_MN_disease, root_state = GM_state(HSMM_MN_disease))
plot_cell_trajectory(HSMM_MN_disease, color_by = "Pseudotime")

##check where each state is located #####
plot_cell_trajectory(HSMM_MN_disease, color_by = "Sample") +
  facet_wrap(~State, nrow = 1)

######################################### genes pseudotimeline #######################################################

##MN_marker_genes <- row.names(subset(fData(HSMM_MN_disease), 
## Gene_Name %in% c())

deathvec.ens2<-as.factor(deathvec.ens)
deathvec.ens2<-deathvec.ens2[!is.na(deathvec.ens2)]

sig_gene_names <- deathvec.ens2
plot_pseudotime_heatmap(HSMM_MN_disease[sig_gene_names,],
                        num_clusters = 4,
                        cores = 1,
                        show_rownames = T)
