# Bioinformatics

## RNA-sequencing analysis pipelines

## Description
The contained scripts are for analysing RNA-seq data.
Pre-processing directory: Shell scripts used for the initial processing of raw NGS data.
Single-cell RNA-seq: R scripts used for the analysis of single-cell RNA-seq data.

## Usage
Pre-processing directory: Workflow stages S1 through S5 are outlined. TPM: Kallisto, an ‘alignment-free’ quantification method, is utilized to normalize raw reads to Transcripts Per Million (TPM) (Bray et al. 2016). The conversion to TPM therefore should be performed after the raw reads have undergone quality checks (S1) and trimming (S2).

Single-cell RNA-seq: The directories contain the following:
Monocle: For performing a single-cell RNA-seq analysis using Monocle.
    monocle_main.R = the main script for analysing scRNA-seq data.
    LoadDataNorm.R = function for filter and normalise data (load with monocle_main.R).
    Classify_Neuron_nonNeuron.R = function to classify according to cell type (in this example neurons and non neuronal populations) (load with monocle_main.R).
    Classify_MN_nonMN_v4.R = function to classify according to cell type (in this example motor neurons and non motor neuron populations) (load with monocle_main.R).
Seurat: For performing a single-cell RNA-seq analysis using Seurat. Workflow is outlined.
DESeq: For performing a differential gene expression analysis on processed single-cell RNA-seq data.
WGCNA: Builds a weighted gene correlation network analysis from processed single-cell RNA-seq data.

## Authors and acknowledgment
Single-cell RNA-seq: Certain scripts written in collaboration with Dr Akshay Bhinge, Dr Ryan Ames, Dr Craig Willis and Dr Rachael Queen.

## Project status
This is an ongoing project.
